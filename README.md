# DFS 

### For run recursive Depth-First Search (DFS) for a matrix
> python dfs/dfs.py

### For run probability distribution
> python dfs/probability_distribution.py

### For test the execution time of the algorithm
> python dfs/test_it.py

#### Matrix 10x10 with probability 0.5
> dfs_runner - 100 loops, best 0.053883 ms per loop, mean 0.121052 ms per loop

> check_percolates - 100 loops, best 0.061989 ms per loop, mean 0.125031 ms per loop

#### Matrix 1000x1000 with probability 0.5
> dfs_runner - 100 loops, best 27.842760 ms per loop, mean 30.568812 ms per loop

> check_percolates - 100 loops, best 29.371977 ms per loop, mean 31.905098 ms per loop


# Fast-DFS

### For run recursive Depth-First Search (DFS) for a matrix
> python fast-dfs/dfs.py

### For run probability distribution
> python fast-dfs/probability_distribution.py

### For test the execution time of the algorithm
> python fast-dfs/test_it.py

#### Matrix 10x10 with probability 0.5
> dfs_runner - 100 loops, best 0.000715 ms per loop, mean 0.034463 ms per loop

> check_percolates - 100 loops, best 0.034094 ms per loop, mean 0.258572 ms per loop

#### Matrix 1000x1000 with probability 0.5
> dfs_runner - 100 loops, best 0.005960 ms per loop, mean 0.045736 ms per loop

> check_percolates - 100 loops, best 26.283979 ms per loop, mean 38.164988 ms per loop

# Percolation
> python percolation_dfs.py

> python percolation_fast_dfs.py



#### Authors: Eugene VORONOV, Hanwen LIU, Andrea CHAHWAN
