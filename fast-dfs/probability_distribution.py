import matplotlib.pyplot as plt
from collections import defaultdict
from numpy import random
import numpy as np
import time


def dfs(graph, square, stack):
    """Recursive Depth-First Search (DFS) for a graph

    :param graph: random matrix based on probability param
    :param square: Points for start search
    :param stack: Stack for store visited squares
    :return: Full stack of squares
    """
    if square not in stack:
        stack.append(square)
        y, x = square[0], square[1]

        for j in range(y-1, y+2):
            for i in range(x-1, x+2):
                if (j < n and j > -1 and i < n  and i > -1) and (graph[j][i] == 0):
                        if (j > y and i == x + 1) or (j < y and i == x + 1) or (j > y and i == x - 1) or (j < y and i == x - 1):
                            continue

                        new_square = (j, i)
                        dfs(graph, new_square, stack)
    return stack


def dfs_runner(graph, square=(0,0)):
    """Execute Depth-First Search (DFS) graph

    :param graph: Generated matrix
    :param square: Points for start search. Default value=(0,0)
    :return: random matrix _graph after dfs when all squares from path was change to the new value=2
    """
    y, x = square[0], square[1]

    if graph[y][x] == 1:
        return False

    stack = dfs(graph, square, [])

    return stack


def check_percolates(graph, square=(0, 0)):
    """Check percolates

    :param open_graph: random matrix based on probability param
    :param square: square
    :return: Boolean
    """
    zeros = np.where(graph[0] == 0)
    for i in zeros[0]:
        n = len(graph)
        stack = dfs_runner(graph, (0, i))

        if stack is False: continue

        for square in stack:
            y, x = square[0], square[1]
            graph[y][x] = 2

        if 2 in graph[n-1]:
            return True
        else:
            continue

    return False


def generate_random_matrix(n, p):
    """Generate random matrix

    :param n: number of squares on the side of the grid
    :param p: generation probability 0 or 1
    :return: random matrix based on probability param
    """
    return random.choice([0, 1], size=(n, n), p=[1-p, p])


def show_probability_distribution_plot():
    """ Show probability distribution plot
    """
    probabilities = np.arange(0, 1.01, 0.01)

    percolates = defaultdict(list)
    percolates_result = []

    for p in probabilities:
        for iter in range(0, iters):
            if p == 0.0:
                percolates[p].append(1)
            else:
                graph = generate_random_matrix(n, p)
                percolate = check_percolates(graph, (0, 0))
                percolates[p].append(percolate)

        percolates_result.append((np.sum(percolates[p])/iters) * 100)

    plt.plot(probabilities, percolates_result, 'go-', label="Probability distribution", linewidth=1, markersize=1)
    plt.xlabel('Probability')
    plt.ylabel('Percolates, %')
    plt.show()


n = 20
square = (0, 0)
stack = []
iters = 100

show_probability_distribution_plot()