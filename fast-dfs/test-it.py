import matplotlib.pyplot as plt
from collections import defaultdict
from numpy import random
import numpy as np
import time


def dfs(graph, square, stack):
    """Recursive Depth-First Search (DFS) for a graph

    :param graph: random matrix based on probability param
    :param square: Points for start search
    :param stack: Stack for store visited squares
    :return: Full stack of squares
    """
    if square not in stack:
        stack.append(square)
        y, x = square[0], square[1]

        for j in range(y-1, y+2):
            for i in range(x-1, x+2):
                if (j < n and j > -1 and i < n  and i > -1) and (graph[j][i] == 0):
                        if (j > y and i == x + 1) or (j < y and i == x + 1) or (j > y and i == x - 1) or (j < y and i == x - 1):
                            continue

                        new_square = (j, i)
                        dfs(graph, new_square, stack)
    return stack


def dfs_runner(graph, square=(0,0)):
    """Execute Depth-First Search (DFS) graph

    :param graph: Generated matrix
    :param square: Points for start search. Default value=(0,0)
    :return: random matrix _graph after dfs when all squares from path was change to the new value=2
    """
    y, x = square[0], square[1]

    if graph[y][x] == 1:
        return False

    stack = dfs(graph, square, [])

    return stack


def check_percolates(graph, square=(0, 0)):
    """Check percolates

    :param open_graph: random matrix based on probability param
    :param square: square
    :return: Boolean
    """
    zeros = np.where(graph[0] == 0)
    for i in zeros[0]:
        n = len(graph)
        stack = dfs_runner(graph, (0, i))

        if stack is False: continue

        for square in stack:
            y, x = square[0], square[1]
            graph[y][x] = 2

        if 2 in graph[n-1]:
            return True
        else:
            continue

    return False


def generate_random_matrix(n, p):
    """Generate random matrix

    :param n: number of squares on the side of the grid
    :param p: generation probability 0 or 1
    :return: random matrix based on probability param
    """
    return random.choice([0, 1], size=(n, n), p=[1-p, p])


def test_dfs_runner():
    """ Test test_dfs_runner function
    """
    times = []

    for iter in range(0, iters):
        graph = generate_random_matrix(n, p)
        ts = time.time()
        dfs_runner(graph, square)
        te = time.time()
        times.append((te - ts) * 1000)

    print('dfs_runner - %i loops, best %f ms per loop, mean %f ms per loop' \
        % (iters, np.min(times, axis=0), np.mean(times, axis=0)))


def test_check_percolates():
    """Test test_check_percolates function
    """
    times = []

    for iter in range(0, iters):
        open_graph = generate_random_matrix(n, p)
        ts = time.time()
        check_percolates(open_graph, (0, 0))
        te = time.time()
        times.append((te - ts) * 1000)

    print('check_percolates - %i loops, best %f ms per loop, mean %f ms per loop' \
        % (iters, np.min(times, axis=0), np.mean(times, axis=0)))


n=1000 # n by n matrix
p=0.5 # Probability
square=(0, 0) # Start point
stack=[]
iters=100

test_dfs_runner()
test_check_percolates()