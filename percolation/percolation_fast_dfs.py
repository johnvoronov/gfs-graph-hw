from numpy import random
import numpy as np
import time


def dfs(graph, square, stack):
    """Recursive Depth-First Search (DFS) for a graph

    :param graph: random matrix based on probability param
    :param square: Points for start search
    :param stack: Stack for store visited squares
    :return: Full stack of squares
    """
    if square not in stack:
        stack.append(square)
        y, x = square[0], square[1]

        for j in range(y-1, y+2):
            for i in range(x-1, x+2):
                if (j < n and j > -1 and i < n  and i > -1) and (graph[j][i] == 0):
                        if (j > y and i == x + 1) or (j < y and i == x + 1) or (j > y and i == x - 1) or (j < y and i == x - 1):
                            continue

                        new_square = (j, i)
                        dfs(graph, new_square, stack)
    return stack

def dfs_runner(graph, square=(0,0)):
    """Execute Depth-First Search (DFS) graph

    :param graph: Generated matrix
    :param square: Points for start search. Default value=(0,0)
    :return: random matrix _graph after dfs when all squares from path was change to the new value=2
    """
    y, x = square[0], square[1]

    if graph[y][x] == 1:
        return False

    stack = dfs(graph, square, [])

    return stack


def check_percolates(graph, square=(0, 0)):
    """Check percolates

    :param open_graph: random matrix based on probability param
    :param square: square
    :return: Boolean
    """
    zeros = np.where(graph[0] == 0)
    for i in zeros[0]:
        n = len(graph)
        stack = dfs_runner(graph, (0, i))

        if stack is False: continue

        for x in range(0, n):
            if graph[n-1][x] == 0 and (n-1, x) in stack:
                for square in stack:
                    graph[square[0]][square[1]] = 2
                return True

    return False

def check_percolates_for_two_points(graph, point_a=(0, 0), point_b=(0, 0)):
    """Check percolates for two points

    :param open_graph: random matrix based on probability param
    :param point_a: point a
    :param point_a: point b
    :return: Boolean
    """

    stack = dfs_runner(graph, point_a)

    if stack is False: return False
    if point_b in stack: return True

    return False


def generate_random_matrix(n, p):
    """Generate random matrix

    :param n: number of squares on the side of the grid
    :param p: generation probability 0 or 1
    :return: random matrix based on probability param
    """
    return random.choice([0, 1], size=(n, n), p=[1-p, p])


def open_random_side(graph, y, x):
    """Open random side in graph

    :param graph: Generated matrix
    :param y: Y coordinate
    :param x: X coordinate
    :return: Updated graph
    """
    graph[y][x] = 0

    return graph


def generate_random_side_coords(open_sides_coords):
    """Generate random side coords for graph

    :param graph: Generated matrix
    :return: X, Y coords
    """
    y = random.randint(0, n)
    x = random.randint(0, n)

    if (y, x) in open_sides_coords:
        return generate_random_side_coords(open_sides_coords)

    return (y, x)


n=5 # n by n matrix
p=1 # Probability
square = (0, 0) # Start point
graph = generate_random_matrix(n, p)
open_sides_coords = []

ts = time.time()
for i in range(0, n*n):
    coords = generate_random_side_coords(open_sides_coords)
    graph = open_random_side(graph, coords[0], coords[1])
    open_sides_coords.append(coords)

    dfs_runner(graph, coords)
    is_percolated = check_percolates(graph)
    if is_percolated:
        print('%i number the random sides we need to open before the system percolates.' % len(open_sides_coords))
        print(graph)
        break

te = time.time()
time = (te - ts) * 1000
print('percolation - best %f ms per loop' % time)
