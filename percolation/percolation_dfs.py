from numpy import random
import numpy as np
import time


def dfs(open_graph, busy_map, y, x):
    """Recursive Depth-First Search (DFS) for a graph

    :param open_graph: random matrix based on probability param
    :param busy_map: random matrix with 0 probability
    :param y: Y coordinate
    :param x: X coordinate
    :return: Busy map
    """
    n = len(open_graph)

    if y < 0 or y >= n: return
    if x < 0 or x >= n: return
    if open_graph[y][x] == 1: return
    if busy_map[y][x] == 0: return

    busy_map[y][x] = 0

    dfs(open_graph, busy_map, y+1, x)
    dfs(open_graph, busy_map, y, x+1)
    dfs(open_graph, busy_map, y, x-1)


def dfs_runner(open_graph):
    """Execute Depth-First Search (DFS) graph

    :param open_graph: random matrix based on probability param
    :return: Busy map
    """
    n = len(open_graph)
    busy_map = generate_random_matrix(n, 1)

    for x, item in enumerate(busy_map):
        dfs(open_graph, busy_map, 0, x)

    return busy_map


def check_percolates(open_graph):
    """Check percolates

    :param open_graph: random matrix based on probability param
    :return: Boolean
    """
    n = len(open_graph)
    busy_map = dfs_runner(open_graph)

    for x in range(0, n):
        if busy_map[n-1][x] == 0:
            return True

    return False


def generate_random_matrix(n, p):
    """Generate random matrix

    :param n: number of squares on the side of the grid
    :param p: generation probability 0 or 1
    :return: random matrix based on probability param
    """
    return random.choice([0, 1], size=(n, n), p=[1-p, p])


def open_random_side(graph, y, x):
    """Open random side in graph

    :param graph: Generated matrix
    :param y: Y coordinate
    :param x: X coordinate
    :return: Updated graph
    """
    graph[y][x] = 0

    return graph


def generate_random_side_coords(open_sides_coords):
    """Generate random side coords for graph

    :param graph: Generated matrix
    :return: X, Y coords
    """
    y = random.randint(0, n)
    x = random.randint(0, n)

    if (y, x) in open_sides_coords:
        return generate_random_side_coords(open_sides_coords)

    return (y, x)


n=5 # n by n matrix
p=0.5 # Probability
square = (0, 0) # Start point
graph = generate_random_matrix(n, p)
open_sides_coords = []

ts = time.time()
for i in range(0, n*n):
    coords = generate_random_side_coords(open_sides_coords)
    graph = open_random_side(graph, coords[0], coords[1])
    open_sides_coords.append(coords)

    dfs_runner(graph)
    is_percolated = check_percolates(graph)
    if is_percolated:
        print('%i number the random sides we need to open before the system percolates.' % len(open_sides_coords))
        break

te = time.time()
time = (te - ts) * 1000
print('percolation - best %f ms per loop' % time)