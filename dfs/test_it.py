import matplotlib.pyplot as plt
from collections import defaultdict
from numpy import random
import numpy as np
import time


def dfs(open_graph, busy_map, y, x):
    """Recursive Depth-First Search (DFS) for a graph

    :param open_graph: random matrix based on probability param
    :param busy_map: random matrix with 0 probability
    :param y: Y coordinate
    :param x: X coordinate
    :return: Busy map
    """
    n = len(open_graph)

    if y < 0 or y >= n: return
    if x < 0 or x >= n: return
    if open_graph[y][x] == 1: return
    if busy_map[y][x] == 0: return

    busy_map[y][x] = 0

    dfs(open_graph, busy_map, y+1, x)
    dfs(open_graph, busy_map, y, x+1)
    dfs(open_graph, busy_map, y, x-1)


def dfs_runner(open_graph):
    """Execute Depth-First Search (DFS) graph

    :param open_graph: random matrix based on probability param
    :return: Busy map
    """
    n = len(open_graph)
    busy_map = generate_random_matrix(n, 1)
    for x, item in enumerate(busy_map):
        dfs(open_graph, busy_map, 0, x)

    return busy_map


def check_percolates(open_graph):
    """Check percolates

    :param open_graph: random matrix based on probability param
    :return: Boolean
    """
    n = len(open_graph)
    busy_map = dfs_runner(open_graph)

    for x in range(0, n):
        if busy_map[n-1][x] == 0: return True

    return False


def generate_random_matrix(n, p):
    """Generate random matrix

    :param n: number of squares on the side of the grid
    :param p: generation probability 0 or 1
    :return: random matrix based on probability param
    """
    return random.choice([0, 1], size=(n, n), p=[1-p, p])


def test_dfs_runner():
    """Test test_dfs_runner function
    """
    times = []

    for iter in range(0, iters):
        open_graph = generate_random_matrix(n, p)
        ts = time.time()
        dfs_runner(open_graph)
        te = time.time()
        times.append((te - ts) * 1000)

    print('dfs_runner - %i loops, best %f ms per loop, mean %f ms per loop' \
        % (iters, np.min(times, axis=0), np.mean(times, axis=0)))

def test_check_percolates():
    """Test test_check_percolates function
    """
    times = []

    for iter in range(0, iters):
        open_graph = generate_random_matrix(n, p)
        ts = time.time()
        check_percolates(open_graph)
        te = time.time()
        times.append((te - ts) * 1000)

    print('check_percolates - %i loops, best %f ms per loop, mean %f ms per loop' \
        % (iters, np.min(times, axis=0), np.mean(times, axis=0)))


n=1000 # n by n matrix
p=0.5 # Probability
square=(0, 0) # Start point
stack=[]
iters=100

test_dfs_runner()
test_check_percolates()