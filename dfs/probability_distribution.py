import matplotlib.pyplot as plt
from collections import defaultdict
from numpy import random
import numpy as np
import time


def dfs(open_graph, busy_map, y, x):
    """Recursive Depth-First Search (DFS) for a graph

    :param open_graph: random matrix based on probability param
    :param busy_map: random matrix with 0 probability
    :param y: Y coordinate
    :param x: X coordinate
    :return: Busy map
    """
    n = len(open_graph)

    if y < 0 or y >= n: return
    if x < 0 or x >= n: return
    if open_graph[y][x] == 1: return
    if busy_map[y][x] == 0: return

    busy_map[y][x] = 0

    dfs(open_graph, busy_map, y+1, x)
    dfs(open_graph, busy_map, y, x+1)
    dfs(open_graph, busy_map, y, x-1)


def dfs_runner(open_graph):
    """Execute Depth-First Search (DFS) graph

    :param open_graph: random matrix based on probability param
    :return: Busy map
    """
    n = len(open_graph)
    busy_map = generate_random_matrix(n, 1)
    for x, item in enumerate(busy_map):
        dfs(open_graph, busy_map, 0, x)

    return busy_map


def check_percolates(open_graph):
    """Check percolates

    :param open_graph: random matrix based on probability param
    :return: Boolean
    """
    n = len(open_graph)
    busy_map = dfs_runner(open_graph)

    for x in range(0, n):
        if busy_map[n-1][x] == 0: return True

    return False


def generate_random_matrix(n, p):
    """Generate random matrix

    :param n: number of squares on the side of the grid
    :param p: generation probability 0 or 1
    :return: random matrix based on probability param
    """
    return random.choice([0, 1], size=(n, n), p=[1-p, p])


def show_probability_distribution_plot():
    """ Show probability distribution plot
    """
    probabilities = np.arange(0, 1.01, 0.01)

    percolates = defaultdict(list)
    percolates_result = []

    for p in probabilities:
        for iter in range(0, iters):
            if p == 0.0:
                percolates[p].append(1)
            else:
                open_graph = generate_random_matrix(n, p)
                percolate = check_percolates(open_graph)
                percolates[p].append(percolate)

        percolates_result.append((np.sum(percolates[p])/iters) * 100)

    plt.plot(probabilities, percolates_result, 'go-', label="Probability distribution", linewidth=1, markersize=1)
    plt.xlabel('Probability')
    plt.ylabel('Percolates, %')
    plt.show()


n = 20
square = (0, 0)
stack = []
iters = 100

show_probability_distribution_plot()